package org.example;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Задача 12.2. Форт Нокс
 * Вы - хакер, волк-одиночка, рыцарь цифрового мира, и в свободное от взлома пентагона время
 * вам пришла новая задача - разграбить федеральное хранилище, избавив толстосумов от залежей желтого металла.
 * Вы должны получить все значения полей федерального хранилища, сохранить эту информацию,
 * установить федеральному хранилищу для всех полей нулевые или пустые значения,
 * и только после этого (!!!) создать собственное хранилище (с помощью приватного конструктора
 * класса Vault) используя награбленные данные в качестве параметров.
 *
 * Т.е. по сути перенести данные из федерального хранилища в свое.
 */
public class Heist {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException, ClassNotFoundException {
        //Создаём клас на базе класса Vault для работы с рефлексией
          Vault federalVault = new Vault();
        //Узнаём все методы класса Vault
          Method[] methods = Vault.class.getDeclaredMethods();
        System.out.println("Методы класса Values:" + Arrays.toString(methods).replace(",", ";\n"));
        System.out.println("-------------------------------------------------------");
        //Узнаём все поля класса Vault
          Field[] fields = Vault.class.getDeclaredFields();
        System.out.println("Поля класса Value" + Arrays.toString(fields).replace(",", ";\n"));

        //Получить и вывести на печать список конструкторов
        System.out.println("********\nКонструкторы: " + Arrays.toString(Vault.class.getDeclaredConstructors())+"\n");

//        try {
//            Method getPentagonNukesCodes = federalVault.getClass().getDeclaredMethod("getPentagonNukesCodes");
//            getPentagonNukesCodes.setAccessible(true);
//            getPentagonNukesCodes.invoke(federalVault);
//        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
//            throw new RuntimeException(e);
//        }
        //Сохраняем себе в переменную ключ от хранилища
        Field pentagonNukesCodes = federalVault.getClass().getDeclaredField("pentagonNukesCodes");
        pentagonNukesCodes.setAccessible(true);
        System.out.println(pentagonNukesCodes.get(federalVault));
        //Сохраняем себе золото
        Field tonsOfGold = federalVault.getClass().getDeclaredField("tonsOfGold");
        tonsOfGold.setAccessible(true);
        System.out.println(tonsOfGold.get(federalVault));
        //Сохраняем себе евро
        Field euro = federalVault.getClass().getDeclaredField("euros");
        euro.setAccessible(true);
        System.out.println(euro.get(federalVault));
        //Сохраняем себе доллары
        Field dollars = federalVault.getClass().getDeclaredField("dollars");
        dollars.setAccessible(true);
        System.out.println(dollars.get(federalVault));
        //Новое значение для доллара
        Field dollarsNewValue = federalVault.getClass().getDeclaredField("dollars");
        dollarsNewValue.setAccessible(true);
        dollarsNewValue.set(federalVault,0);
        System.out.println(dollarsNewValue.get(federalVault));
        //Новое значение для евро
        Field eurosNewValues = federalVault.getClass().getDeclaredField("euros");
        eurosNewValues.setAccessible(true);
        eurosNewValues.set(federalVault,0);
        System.out.println(eurosNewValues.get(federalVault));
        //Новое значение для золота
        Field tonsOfGoldNewValue = federalVault.getClass().getDeclaredField("tonsOfGold");
        tonsOfGoldNewValue.setAccessible(true);
        tonsOfGoldNewValue.set(federalVault,0.0);
        System.out.println(tonsOfGoldNewValue.get(federalVault));
        //Новое значение для ключа хранилища
        Field pentagonNukesCodesNewValue = federalVault.getClass().getDeclaredField("pentagonNukesCodes");
        pentagonNukesCodesNewValue.setAccessible(true);
        pentagonNukesCodesNewValue.set(federalVault, null);
        System.out.println(pentagonNukesCodesNewValue.get(federalVault));

        //Создаём объект класса Vault через рефлексию
        Constructor value = federalVault.getClass().getDeclaredConstructor( int.class, int.class, double.class, String.class);
        value.setAccessible(true);
        Object newVault = value.newInstance(1000000,300000,21.5,"qwerty");
        System.out.println(newVault);

//        Class vaultClass = Class.forName("org.example.Vault");
//        Constructor<Vault> constructor = vaultClass.getDeclaredConstructor(int.class, int.class, double.class, String.class);
//        Vault objectVault = constructor.newInstance(1000000,300000,21.5,"qwerty");





        // ???
        //PROFIT!

    }
}
